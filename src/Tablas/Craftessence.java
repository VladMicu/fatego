/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tablas;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Miky
 */
@Entity
@Table(name = "CRAFTESSENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Craftessence.findAll", query = "SELECT c FROM Craftessence c")
    , @NamedQuery(name = "Craftessence.findById", query = "SELECT c FROM Craftessence c WHERE c.id = :id")
    , @NamedQuery(name = "Craftessence.findByName", query = "SELECT c FROM Craftessence c WHERE c.name = :name")
    , @NamedQuery(name = "Craftessence.findByLevel", query = "SELECT c FROM Craftessence c WHERE c.level = :level")
    , @NamedQuery(name = "Craftessence.findByAquired", query = "SELECT c FROM Craftessence c WHERE c.aquired = :aquired")
    , @NamedQuery(name = "Craftessence.findByDate", query = "SELECT c FROM Craftessence c WHERE c.date = :date")
    , @NamedQuery(name = "Craftessence.findByAttack", query = "SELECT c FROM Craftessence c WHERE c.attack = :attack")
    , @NamedQuery(name = "Craftessence.findByHp", query = "SELECT c FROM Craftessence c WHERE c.hp = :hp")
    , @NamedQuery(name = "Craftessence.findByEffect", query = "SELECT c FROM Craftessence c WHERE c.effect = :effect")
    , @NamedQuery(name = "Craftessence.findByPic", query = "SELECT c FROM Craftessence c WHERE c.pic = :pic")})
public class Craftessence implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "LEVEL")
    private String level;
    @Column(name = "AQUIRED")
    private Boolean aquired;
    @Column(name = "DATE")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "ATTACK")
    private String attack;
    @Column(name = "HP")
    private String hp;
    @Column(name = "EFFECT")
    private String effect;
    @Column(name = "PIC")
    private String pic;
    @OneToMany(mappedBy = "craftessence")
    private Collection<Servant> servantCollection;

    public Craftessence() {
    }

    public Craftessence(Integer id) {
        this.id = id;
    }

    public Craftessence(Integer id, String name, String level) {
        this.id = id;
        this.name = name;
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Boolean getAquired() {
        return aquired;
    }

    public void setAquired(Boolean aquired) {
        this.aquired = aquired;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getPicCE() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @XmlTransient
    public Collection<Servant> getServantCollection() {
        return servantCollection;
    }

    public void setServantCollection(Collection<Servant> servantCollection) {
        this.servantCollection = servantCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Craftessence)) {
            return false;
        }
        Craftessence other = (Craftessence) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Tablas.Craftessence[ id=" + id + " ]";
    }
    
}
