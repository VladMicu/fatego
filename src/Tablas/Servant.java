/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tablas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Miky
 */
@Entity
@Table(name = "SERVANT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servant.findAll", query = "SELECT s FROM Servant s")
    , @NamedQuery(name = "Servant.findById", query = "SELECT s FROM Servant s WHERE s.id = :id")
    , @NamedQuery(name = "Servant.findByName", query = "SELECT s FROM Servant s WHERE s.name = :name")
    , @NamedQuery(name = "Servant.findByLevel", query = "SELECT s FROM Servant s WHERE s.level = :level")
    , @NamedQuery(name = "Servant.findByAscension", query = "SELECT s FROM Servant s WHERE s.ascension = :ascension")
    , @NamedQuery(name = "Servant.findByNp", query = "SELECT s FROM Servant s WHERE s.np = :np")
    , @NamedQuery(name = "Servant.findByNplevel", query = "SELECT s FROM Servant s WHERE s.nplevel = :nplevel")
    , @NamedQuery(name = "Servant.findByAquired", query = "SELECT s FROM Servant s WHERE s.aquired = :aquired")
    , @NamedQuery(name = "Servant.findByDate", query = "SELECT s FROM Servant s WHERE s.date = :date")
    , @NamedQuery(name = "Servant.findByAttack", query = "SELECT s FROM Servant s WHERE s.attack = :attack")
    , @NamedQuery(name = "Servant.findByHp", query = "SELECT s FROM Servant s WHERE s.hp = :hp")
    , @NamedQuery(name = "Servant.findByStarab", query = "SELECT s FROM Servant s WHERE s.starab = :starab")
    , @NamedQuery(name = "Servant.findByStargen", query = "SELECT s FROM Servant s WHERE s.stargen = :stargen")
    , @NamedQuery(name = "Servant.findByNpAtk", query = "SELECT s FROM Servant s WHERE s.npAtk = :npAtk")
    , @NamedQuery(name = "Servant.findByNpDef", query = "SELECT s FROM Servant s WHERE s.npDef = :npDef")
    , @NamedQuery(name = "Servant.findByDeathresist", query = "SELECT s FROM Servant s WHERE s.deathresist = :deathresist")
    , @NamedQuery(name = "Servant.findByPic", query = "SELECT s FROM Servant s WHERE s.pic = :pic")})
public class Servant implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "LEVEL")
    private String level;
    @Column(name = "ASCENSION")
    private Character ascension;
    @Column(name = "NP")
    private String np;
    @Column(name = "NPLEVEL")
    private Character nplevel;
    @Column(name = "AQUIRED")
    private Boolean aquired;
    @Column(name = "DATE")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "ATTACK")
    private String attack;
    @Column(name = "HP")
    private String hp;
    @Column(name = "STARAB")
    private String starab;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "STARGEN")
    private BigDecimal stargen;
    @Column(name = "NP_ATK")
    private BigDecimal npAtk;
    @Column(name = "NP_DEF")
    private BigDecimal npDef;
    @Column(name = "DEATHRESIST")
    private String deathresist;
    @Column(name = "PIC")
    private String pic;
    @JoinColumn(name = "CLASS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Class class1;
    @JoinColumn(name = "CRAFTESSENCE", referencedColumnName = "ID")
    @ManyToOne
    private Craftessence craftessence;

    public Servant() {
    }

    public Servant(Integer id) {
        this.id = id;
    }

    public Servant(Integer id, String name, String level) {
        this.id = id;
        this.name = name;
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Character getAscension() {
        return ascension;
    }

    public void setAscension(Character ascension) {
        this.ascension = ascension;
    }

    public String getNp() {
        return np;
    }

    public void setNp(String np) {
        this.np = np;
    }

    public Character getNplevel() {
        return nplevel;
    }

    public void setNplevel(Character nplevel) {
        this.nplevel = nplevel;
    }

    public Boolean getAquired() {
        return aquired;
    }

    public void setAquired(Boolean aquired) {
        this.aquired = aquired;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getStarab() {
        return starab;
    }

    public void setStarab(String starab) {
        this.starab = starab;
    }

    public BigDecimal getStargen() {
        return stargen;
    }

    public void setStargen(BigDecimal stargen) {
        this.stargen = stargen;
    }

    public BigDecimal getNpAtk() {
        return npAtk;
    }

    public void setNpAtk(BigDecimal npAtk) {
        this.npAtk = npAtk;
    }

    public BigDecimal getNpDef() {
        return npDef;
    }

    public void setNpDef(BigDecimal npDef) {
        this.npDef = npDef;
    }

    public String getDeathresist() {
        return deathresist;
    }

    public void setDeathresist(String deathresist) {
        this.deathresist = deathresist;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Class getClass1() {
        return class1;
    }

    public void setClass1(Class class1) {
        this.class1 = class1;
    }

    public Craftessence getCraftessence() {
        return craftessence;
    }

    public void setCraftessence(Craftessence craftessence) {
        this.craftessence = craftessence;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servant)) {
            return false;
        }
        Servant other = (Servant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Tablas.Servant[ id=" + id + " ]";
    }
    
}
