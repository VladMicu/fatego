package fatego;

import Tablas.Craftessence;
import Tablas.Servant;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

/**
 * FXML Controller class
 */
public class CEEditController implements Initializable {

    public static final String FOLDER_CE_PICS = "CEPics";
    private TableView tableViewPrevious;
    private Pane rootCEView;
    private EntityManager entityManager;
    private Servant servant;
    private boolean newCE;
    private Craftessence CE;

    @FXML
    private TextField textFieldCEName;
    @FXML
    private TextField textFieldCELevel;
    @FXML
    private TextField textFieldATK;
    @FXML
    private TextField textFieldHP;
    @FXML
    private CheckBox checkBoxAquired;
    @FXML
    private TextArea textAreaEffect;
    @FXML
    private ImageView imageViewCE;
    @FXML
    private AnchorPane rootCEEdit;
    @FXML
    private DatePicker datePickerCE;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void setCE(EntityManager entityManager, Craftessence craft, boolean newCE) {
        this.entityManager = entityManager;
        entityManager.getTransaction().begin();
        if (!newCE) {
            this.CE = entityManager.find(Craftessence.class, craft.getId());
        } else {
            this.CE = craft;
        }
        this.newCE = newCE;
    }

    public void setRootCEView(Pane rootCEView) {
        this.rootCEView = rootCEView;
    }

    @FXML
    private void onActionButtonSave(ActionEvent event) {
        boolean errorFormato = false;
        if (!errorFormato) {  
            try {
                CE.setName(textFieldCEName.getText());
                CE.setLevel(textFieldCELevel.getText());
                CE.setAttack(textFieldATK.getText());
                CE.setHp(textFieldHP.getText());
                CE.setEffect(textAreaEffect.getText());
                CE.setAquired(checkBoxAquired.isSelected());
                if (datePickerCE.getValue() != null) {
                    LocalDate localDate = datePickerCE.getValue();
                    ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
                    Instant instant = zonedDateTime.toInstant();
                    Date date = Date.from(instant);
                    CE.setDate(date);
                } else {
                    CE.setDate(null);
                }

                if (newCE) {
                    entityManager.persist(CE);
                } else {
                    entityManager.merge(CE);
                }
                entityManager.getTransaction().commit();
                StackPane rootMain = (StackPane) rootCEView.getScene().getRoot();
                rootMain.getChildren().remove(rootCEEdit);

                rootCEView.setVisible(true);

                int numFilaSeleccionada;
                if (newCE) {
                    tableViewPrevious.getItems().add(CE);
                    numFilaSeleccionada = tableViewPrevious.getItems().size() - 1;
                    tableViewPrevious.getSelectionModel().select(numFilaSeleccionada);
                    tableViewPrevious.scrollTo(numFilaSeleccionada);
                } else {
                    numFilaSeleccionada = tableViewPrevious.getSelectionModel().getSelectedIndex();
                    tableViewPrevious.getItems().set(numFilaSeleccionada, CE);
                }
                TablePosition pos = new TablePosition(tableViewPrevious, numFilaSeleccionada, null);
                tableViewPrevious.getFocusModel().focus(pos);
                tableViewPrevious.requestFocus();
            } catch (RollbackException ex) { // Los datos introducidos no cumplen los requisitos de la BD
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Changes couldn't be saved. "
                        + "Check if the data is correct.");
                alert.setContentText(ex.getLocalizedMessage());
                alert.showAndWait();
            }
        }
    }

    @FXML
    private void onActionButtonCancel(ActionEvent event) {
        entityManager.getTransaction().rollback();

        int selectedRow = tableViewPrevious.getSelectionModel().getSelectedIndex();
        StackPane rootMain = (StackPane) rootCEView.getScene().getRoot();
        rootMain.getChildren().remove(rootCEEdit);
        TablePosition pos = new TablePosition(tableViewPrevious, selectedRow, null);

        rootCEView.setVisible(true);
    }

    public void setTableViewPrevious(TableView tableViewPrevious) {
        this.tableViewPrevious = tableViewPrevious;
    }

    @FXML
    private void onActionButtonPic(ActionEvent event
    ) {
        File carpetaFotos = new File(FOLDER_CE_PICS);
        if (!carpetaFotos.exists()) {
            carpetaFotos.mkdir();
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Chose picture");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Images (jpg, png)", "*.jpg", "*.png"),
                new FileChooser.ExtensionFilter("All", "*.*")
        );
        File file = fileChooser.showOpenDialog(rootCEEdit.getScene().getWindow());
        if (file != null) {
            try {
                Files.copy(file.toPath(), new File(FOLDER_CE_PICS + "/" + file.getName()).toPath());
                CE.setPic(file.getName());
                Image image = new Image(file.toURI().toString());
                imageViewCE.setImage(image);
            } catch (FileAlreadyExistsException ex) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "File name already exists");
                alert.showAndWait();
            } catch (IOException ex) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Image couldn't be saved");
                alert.showAndWait();
            }
        }

    }

    @FXML
    private void onActionButtonDelete(ActionEvent event
    ) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm delete");
        alert.setHeaderText("Really want to delete the image, \n"
                + "Keep the file, \no ABORT");
        alert.setContentText("Chose your option:");

        ButtonType buttonTypeEliminar = new ButtonType("Delete");
        ButtonType buttonTypeMantener = new ButtonType("Keep");
        ButtonType buttonTypeCancel = new ButtonType("ABORT", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeEliminar, buttonTypeMantener, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeEliminar) {
            String imageFileName = CE.getPicCE();
            File file = new File(FOLDER_CE_PICS + "/" + imageFileName);
            if (file.exists()) {
                file.delete();
            }
            CE.setPic(null);
            imageViewCE.setImage(null);
        } else if (result.get() == buttonTypeMantener) {
            servant.setPic(null);
            imageViewCE.setImage(null);
        }
    }

    public void showCEData() {
        textFieldCEName.setText(CE.getName());
        textFieldCELevel.setText(CE.getLevel());
        textFieldATK.setText(CE.getAttack());
        textFieldHP.setText(CE.getHp());
        textAreaEffect.setText(CE.getEffect());
        if (CE.getAquired() != null) {
            checkBoxAquired.setSelected(CE.getAquired());
        }
        if (CE.getDate() != null) {
            Date date = CE.getDate();
            Instant instant = date.toInstant();
            ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
            LocalDate localDate = zdt.toLocalDate();
            datePickerCE.setValue(localDate);
        }
        if (CE.getPicCE() != null) {
            String imageFileName = CE.getPicCE();
            File file = new File(FOLDER_CE_PICS + "/" + imageFileName);
            if (file.exists()) {
                Image image = new Image(file.toURI().toString());
                imageViewCE.setImage(image);
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Picture not found !!!");
                alert.showAndWait();
            }
        }
    }

}
