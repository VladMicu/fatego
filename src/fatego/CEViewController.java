package fatego;

import Tablas.Craftessence;
import static fatego.CEEditController.FOLDER_CE_PICS;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;

/**
 * FXML Controller class
 */
public class CEViewController implements Initializable {

    private boolean newCE;
    private TableView tableViewPrevious;
    private Pane rootServantView;
    private Craftessence selectedCE;
    private EntityManager entityManager;

    @FXML
    private AnchorPane rootCEView;
    @FXML
    private TableView<Craftessence> tableViewCE;
    @FXML
    private TableColumn<Craftessence, String> columnCEName;
    @FXML
    private TableColumn<Craftessence, String> columnCEATK;
    @FXML
    private TableColumn<Craftessence, String> columnCEHP;
    @FXML
    private TableColumn<Craftessence, String> columnCELevel;
    @FXML
    private TableColumn<Craftessence, String> columnEffect;
    @FXML
    private Label labelCEName;
    @FXML
    private Label labelCELevel;
    @FXML
    private Label labelCEAquired;
    @FXML
    private Label labelEffect;
    @FXML
    private ImageView imageViewCE2;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        columnCEName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnCEATK.setCellValueFactory(new PropertyValueFactory<>("attack"));
        columnCEHP.setCellValueFactory(new PropertyValueFactory<>("hp"));
        columnCELevel.setCellValueFactory(new PropertyValueFactory<>("level"));
        columnEffect.setCellValueFactory(new PropertyValueFactory<>("effect"));
        
        
        
        tableViewCE.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectedCE = newValue;
                    if (selectedCE != null) {
                        labelCEName.setText(selectedCE.getName());
                        labelCELevel.setText(selectedCE.getLevel());
                        labelEffect.setText(selectedCE.getEffect());                        
                        labelCEAquired.setText(selectedCE.getAquired().toString());

                        if (selectedCE.getPicCE() != null) {
                            String imageFileName = selectedCE.getPicCE();
                            File file = new File(FOLDER_CE_PICS + "/" + imageFileName);
                            if (file.exists()) {
                                Image image = new Image(file.toURI().toString());
                                imageViewCE2.setImage(image);
                            } else {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Picture not found !!!");
                                alert.showAndWait();
                            }
                        } else {
                            String imageFileNameCE = selectedCE.getPicCE();
                            File file = new File(FOLDER_CE_PICS + "/" + imageFileNameCE);
                            Image imageCE = new Image(file.toURI().toString());
                            imageViewCE2.setImage(null);
                        }
                    } else {
                        labelCEName.setText("");
                        labelCELevel.setText("");
                        labelEffect.setText("");
                        labelCEAquired.setText("");
                    }

                });
    }

    public void loadAllCEs() {
        Query queryCraftessenceFindAll = entityManager.createNamedQuery("Craftessence.findAll");
        List<Craftessence> listCEs = queryCraftessenceFindAll.getResultList();
        tableViewCE.setItems(FXCollections.observableArrayList(listCEs));
    }

    @FXML
    private void onActionButtonNewCE(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CEEdit.fxml"));
            Parent rootCEEdit = fxmlLoader.load();

            CEEditController ceEditController = (CEEditController) fxmlLoader.getController();
            ceEditController.setTableViewPrevious(tableViewCE);
            ceEditController.setRootCEView(rootCEView);
            selectedCE = new Craftessence();
            ceEditController.setCE(entityManager, selectedCE, true);
            ceEditController.showCEData();
            
            rootCEView.setVisible(false);

            StackPane rootMain = (StackPane) rootCEView.getScene().getRoot();
            rootMain.getChildren().add(rootCEEdit);
        } catch (IOException ex) {
            Logger.getLogger(CEViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onActionButtonEditCE(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CEEdit.fxml"));
            Parent rootCEEdit = fxmlLoader.load();

            CEEditController CEEditController = (CEEditController) fxmlLoader.getController();
            CEEditController.setRootCEView(rootCEView);
            CEEditController.setTableViewPrevious(tableViewCE);
            CEEditController.setCE(entityManager, selectedCE, false);
            CEEditController.showCEData();

            rootCEView.setVisible(false);

            StackPane rootMain = (StackPane) rootCEView.getScene().getRoot();
            rootMain.getChildren().add(rootCEEdit);
        } catch (IOException ex) {
            Logger.getLogger(CEViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onActionButtonDeleteCE(ActionEvent event) {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm");
            alert.setHeaderText("Really want to scrap this CE ?");
            alert.setContentText(selectedCE.getName());
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                entityManager.getTransaction().begin();
                entityManager.merge(selectedCE);
                entityManager.remove(selectedCE);
                entityManager.getTransaction().commit();

                tableViewCE.getItems().remove(selectedCE);

                tableViewCE.getFocusModel().focus(null);
                tableViewCE.requestFocus();
            } else {
                int numFilaSeleccionada = tableViewCE.getSelectionModel().getSelectedIndex();
                tableViewCE.getItems().set(numFilaSeleccionada, selectedCE);
                TablePosition pos = new TablePosition(tableViewCE, numFilaSeleccionada, null);
                tableViewCE.getFocusModel().focus(pos);
                tableViewCE.requestFocus();
            }
        } catch (RollbackException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Cant delete this. It's used.");
            alert.setContentText(selectedCE.getName());
            Optional<ButtonType> result = alert.showAndWait();
        }
    }

    @FXML
    private void onActionButtonServant(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ServantView.fxml"));
            Parent rootServantView = fxmlLoader.load();
            ServantViewController ServView = (ServantViewController) fxmlLoader.getController();
            ServView.setEntityManager(entityManager);
            ServView.loadAllServants();
            
            rootCEView.setVisible(false);

            Pane rootMain = (Pane) rootCEView.getScene().getRoot();
            rootMain.getChildren().add(rootServantView);
        } catch (IOException ex) {
            Logger.getLogger(CEViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
