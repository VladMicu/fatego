package FateGO;


import fatego.ServantViewController;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FateGO extends Application {

    private EntityManagerFactory emf;
    private EntityManager em;

    @Override
    public void start(Stage primaryStage) throws IOException {
        StackPane rootMain = new StackPane();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ServantView.fxml"));
        Pane rootServantView = fxmlLoader.load();
        rootMain.getChildren().add(rootServantView);

        emf = Persistence.createEntityManagerFactory("FateGOPU");
        em = emf.createEntityManager();

        ServantViewController servantViewController = (ServantViewController) fxmlLoader.getController();
        servantViewController.setEntityManager(em);
        servantViewController.loadAllServants();
        Scene scene = new Scene(rootMain, 700, 650);

        primaryStage.setTitle("F/GO BD");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    @Override
    public void stop() throws Exception {
        em.close();
        emf.close();
        try {
            DriverManager.getConnection("jdbc:derby:BD_Fate;shutdown=true");
        } catch (SQLException ex) {
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
