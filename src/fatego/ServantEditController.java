package fatego;

import Tablas.Servant;
import Tablas.Class;
import Tablas.Craftessence;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;

/**
 * FXML Controller class
 */
public class ServantEditController implements Initializable {

    public static final char A1 = '1';
    public static final char A2 = '2';
    public static final char A3 = '3';
    public static final char A4 = '4';
    public static final char A0 = '0';
    public static final char NP1 = '1';
    public static final char NP2 = '2';
    public static final char NP3 = '3';
    public static final char NP4 = '4';
    public static final char NP5 = '5';

    public static final String FOLDER_SERV_PICS = "ServantPics";
    private TableView tableViewPrevious;
    private Pane rootServantsView;
    private EntityManager entityManager;
    private Servant servant;
    private boolean newServant;

    @FXML
    private AnchorPane rootServantEdit;
    @FXML
    private DatePicker datePickerAquisition;
    @FXML
    private TextField textFieldName;
    @FXML
    private TextField textFieldNP;
    @FXML
    private TextField textFieldLevel;
    @FXML
    private ComboBox<Class> comboBoxClass;
    @FXML
    private ComboBox<Craftessence> comboBoxCE;
    @FXML
    private CheckBox checkBoxAquired;
    @FXML
    private RadioButton radioButtonA0;
    @FXML
    private RadioButton radioButtonA1;
    @FXML
    private RadioButton radioButtonA2;
    @FXML
    private RadioButton radioButtonA3;
    @FXML
    private RadioButton radioButtonA4;
    @FXML
    private RadioButton radioButtonNP1;
    @FXML
    private RadioButton radioButtonNP2;
    @FXML
    private RadioButton radioButtonNP3;
    @FXML
    private RadioButton radioButtonNP4;
    @FXML
    private RadioButton radioButtonNP5;
    @FXML
    private ImageView imageViewServant;
    @FXML
    private TextField textFieldHP;
    @FXML
    private TextField textFieldATK;
    @FXML
    private TextField textFieldStAb;
    @FXML
    private TextField textFieldStGen;
    @FXML
    private TextField textFieldANP;
    @FXML
    private TextField textFieldDNP;
    @FXML
    private TextField textFieldDResist;
    @FXML
    private ToggleGroup Ascension;
    @FXML
    private ToggleGroup NPL;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void setServant(EntityManager entityManager, Servant servant, boolean newServant) {
        this.entityManager = entityManager;
        entityManager.getTransaction().begin();
        if (!newServant) {
            this.servant = entityManager.find(Servant.class, servant.getId());
        } else {
            this.servant = servant;
        }
        this.newServant = newServant;
    }

    public void setRootServantsView(Pane rootServantsView) {
        this.rootServantsView = rootServantsView;
    }

    @FXML
    private void onActionButtonAddPic(ActionEvent event) {
        File carpetaFotos = new File(FOLDER_SERV_PICS);
        if (!carpetaFotos.exists()) {
            carpetaFotos.mkdir();
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Chose picture");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Images (jpg, png)", "*.jpg", "*.png"),
                new FileChooser.ExtensionFilter("All", "*.*")
        );
        File file = fileChooser.showOpenDialog(rootServantEdit.getScene().getWindow());
        if (file != null) {
            try {
                Files.copy(file.toPath(), new File(FOLDER_SERV_PICS + "/" + file.getName()).toPath());
                servant.setPic(file.getName());
                Image image = new Image(file.toURI().toString());
                imageViewServant.setImage(image);
            } catch (FileAlreadyExistsException ex) {
                Alert alert = new Alert(AlertType.WARNING, "File name already exists");
                alert.showAndWait();
            } catch (IOException ex) {
                Alert alert = new Alert(AlertType.WARNING, "Image couldn't be saved");
                alert.showAndWait();
            }
        }

    }

    @FXML

    private void onActionButtonDelete(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirm delete");
        alert.setHeaderText("Really want to delete the image, \n"
                + "Keep the file, \no ABORT");
        alert.setContentText("Chose your option:");

        ButtonType buttonTypeEliminar = new ButtonType("Delete");
        ButtonType buttonTypeMantener = new ButtonType("Keep");
        ButtonType buttonTypeCancel = new ButtonType("ABORT", ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeEliminar, buttonTypeMantener, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeEliminar) {
            String imageFileName = servant.getPic();
            File file = new File(FOLDER_SERV_PICS + "/" + imageFileName);
            if (file.exists()) {
                file.delete();
            }
            servant.setPic(null);
            imageViewServant.setImage(null);
        } else if (result.get() == buttonTypeMantener) {
            servant.setPic(null);
            imageViewServant.setImage(null);
        }

    }

    @FXML
    private void onActionButtonSave(ActionEvent event) {
        boolean errorFormato = false;
        if (!errorFormato) {  
            try {
                servant.setName(textFieldName.getText());
                servant.setLevel(textFieldLevel.getText());
                servant.setNp(textFieldNP.getText());
                servant.setHp(textFieldHP.getText());
                servant.setAttack(textFieldATK.getText());
                servant.setStarab(textFieldStAb.getText());
                if (!textFieldStGen.getText().isEmpty()) {
                    try {
                        servant.setStargen(BigDecimal.valueOf(Double.valueOf(textFieldANP.getText()).doubleValue()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "Value not valid");
                        alert.showAndWait();
                        textFieldStGen.requestFocus();
                    }
                }
                servant.setDeathresist(textFieldDResist.getText());
                if (!textFieldANP.getText().isEmpty()) {
                    try {
                        servant.setNpAtk(BigDecimal.valueOf(Double.valueOf(textFieldANP.getText()).doubleValue()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "Value not valid");
                        alert.showAndWait();
                        textFieldANP.requestFocus();
                    }
                }
                if (!textFieldDNP.getText().isEmpty()) {
                    try {
                        servant.setNpDef(BigDecimal.valueOf(Double.valueOf(textFieldDNP.getText()).doubleValue()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "Value not valid");
                        alert.showAndWait();
                        textFieldDNP.requestFocus();
                    }
                }
                if (comboBoxCE.getValue() != null) {
                    servant.setCraftessence(comboBoxCE.getValue());
                }
                if (comboBoxClass.getValue() != null) {
                    servant.setClass1(comboBoxClass.getValue());
                } else {
                    Alert alert = new Alert(AlertType.INFORMATION, "Must select a Class");
                    alert.showAndWait();
                    errorFormato = true;
                }

                servant.setAquired(checkBoxAquired.isSelected());

                if (radioButtonA0.isSelected()) {
                    servant.setAscension(A0);
                } else if (radioButtonA1.isSelected()) {
                    servant.setAscension(A1);
                } else if (radioButtonA2.isSelected()) {
                    servant.setAscension(A2);
                } else if (radioButtonA3.isSelected()) {
                    servant.setAscension(A3);
                } else if (radioButtonA4.isSelected()) {
                    servant.setAscension(A4);
                }

                if (radioButtonNP1.isSelected()) {
                    servant.setNplevel(NP1);
                } else if (radioButtonNP2.isSelected()) {
                    servant.setNplevel(NP2);
                } else if (radioButtonNP3.isSelected()) {
                    servant.setNplevel(NP3);
                } else if (radioButtonNP4.isSelected()) {
                    servant.setNplevel(NP4);
                } else if (radioButtonNP5.isSelected()) {
                    servant.setNplevel(NP5);
                }

                if (datePickerAquisition.getValue() != null) {
                    LocalDate localDate = datePickerAquisition.getValue();
                    ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
                    Instant instant = zonedDateTime.toInstant();
                    Date date = Date.from(instant);
                    servant.setDate(date);
                } else {
                    servant.setDate(null);
                }
                if (newServant) {
                    entityManager.persist(servant);
                } else {
                    entityManager.merge(servant);
                }
                entityManager.getTransaction().commit();
                StackPane rootMain = (StackPane) rootServantsView.getScene().getRoot();
                rootMain.getChildren().remove(rootServantEdit);

                rootServantsView.setVisible(true);

                int numFilaSeleccionada;
                if (newServant) {
                    tableViewPrevious.getItems().add(servant);
                    numFilaSeleccionada = tableViewPrevious.getItems().size() - 1;
                    tableViewPrevious.getSelectionModel().select(numFilaSeleccionada);
                    tableViewPrevious.scrollTo(numFilaSeleccionada);
                } else {
                    numFilaSeleccionada = tableViewPrevious.getSelectionModel().getSelectedIndex();
                    tableViewPrevious.getItems().set(numFilaSeleccionada, servant);
                }
                TablePosition pos = new TablePosition(tableViewPrevious, numFilaSeleccionada, null);
                tableViewPrevious.getFocusModel().focus(pos);
                tableViewPrevious.requestFocus();
            } catch (RollbackException ex) { // Los datos introducidos no cumplen los requisitos de la BD
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setHeaderText("Changes couldn't be saved. "
                        + "Check if the data is correct.");
                alert.setContentText(ex.getLocalizedMessage());
                alert.showAndWait();
            }
        }

    }

    @FXML
    private void onActionButtonCancel(ActionEvent event) {
        entityManager.getTransaction().rollback();

        int selectedRow = tableViewPrevious.getSelectionModel().getSelectedIndex();
        StackPane rootMain = (StackPane) rootServantsView.getScene().getRoot();
        rootMain.getChildren().remove(rootServantEdit);
        TablePosition pos = new TablePosition(tableViewPrevious, selectedRow, null);

        rootServantsView.setVisible(true);

    }

    public void setTableViewPrevious(TableView tableViewPrevious) {
        this.tableViewPrevious = tableViewPrevious;
    }

    public void showServantData() {
        textFieldName.setText(servant.getName());
        textFieldLevel.setText(servant.getLevel());
        textFieldNP.setText(servant.getNp());
        textFieldHP.setText(servant.getHp());
        textFieldATK.setText(servant.getAttack());
        textFieldStAb.setText(servant.getStarab());
        if (servant.getStargen() != null) {
            textFieldStGen.setText(servant.getStargen().toString());
        }
        if (servant.getNpAtk() != null) {
            textFieldANP.setText(servant.getNpAtk().toString());
        }
        if (servant.getNpDef() != null) {
            textFieldDNP.setText(servant.getNpDef().toString());
        }
        textFieldDResist.setText(servant.getDeathresist());
        if (servant.getAscension() != null) {
            switch (servant.getAscension()) {
                case A0:
                    radioButtonA0.setSelected(true);
                    break;
                case A1:
                    radioButtonA1.setSelected(true);
                    break;
                case A2:
                    radioButtonA2.setSelected(true);
                    break;
                case A3:
                    radioButtonA3.setSelected(true);
                    break;
                case A4:
                    radioButtonA4.setSelected(true);
                    break;
            }
        } else {
            radioButtonA0.setSelected(true);
        }
        if (servant.getNplevel() != null) {
            switch (servant.getNplevel()) {
                case NP1:
                    radioButtonNP1.setSelected(true);
                    break;
                case NP2:
                    radioButtonNP2.setSelected(true);
                    break;
                case NP3:
                    radioButtonNP3.setSelected(true);
                    break;
                case NP4:
                    radioButtonNP4.setSelected(true);
                    break;
                case NP5:
                    radioButtonNP5.setSelected(true);
                    break;
            }
        } else {
            radioButtonNP1.setSelected(true);
        }
        if (servant.getAquired() != null) {
            checkBoxAquired.setSelected(servant.getAquired());
        }
        if (servant.getDate() != null) {
            Date date = servant.getDate();
            Instant instant = date.toInstant();
            ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
            LocalDate localDate = zdt.toLocalDate();
            datePickerAquisition.setValue(localDate);
        }
        Query queryCEFindAll = entityManager.createNamedQuery("Craftessence.findAll");
        List listCE = queryCEFindAll.getResultList();
        comboBoxCE.setItems(FXCollections.observableList(listCE));
        if (servant.getName() != null) {
            comboBoxCE.setValue(servant.getCraftessence());
        } else {
            comboBoxCE.setValue(null);
        }

        comboBoxCE.setCellFactory((ListView<Craftessence> l) -> new ListCell<Craftessence>() {
            @Override
            protected void updateItem(Craftessence CE, boolean empty) {
                super.updateItem(CE, empty);
                if (CE == null || empty) {
                    setText("");
                } else {
                    setText(CE.getName());
                }
            }
        });
        comboBoxCE.setConverter(new StringConverter<Craftessence>() {
            @Override
            public String toString(Craftessence CE) {
                if (CE == null) {
                    return null;
                } else {
                    return CE.getName();
                }
            }

            @Override
            public Craftessence fromString(String userId) {
                return null;
            }
        });

        Query queryClassFindAll = entityManager.createNamedQuery("Class.findAll");
        List listClass = queryClassFindAll.getResultList();
        comboBoxClass.setItems(FXCollections.observableList(listClass));
        if (servant.getName() != null) {
            comboBoxClass.setValue(servant.getClass1());
        }

        comboBoxClass.setCellFactory((ListView<Class> l) -> new ListCell<Class>() {
            @Override
            protected void updateItem(Class classes, boolean empty) {
                super.updateItem(classes, empty);
                if (classes == null || empty) {
                    setText("");
                } else {
                    setText(classes.getNombre());
                }
            }
        });
        comboBoxClass.setConverter(new StringConverter<Class>() {
            @Override
            public String toString(Class classes) {
                if (classes == null) {
                    return null;
                } else {
                    return classes.getNombre();
                }
            }

            @Override
            public Class fromString(String userId) {
                return null;
            }
        });

        if (servant.getPic() != null) {
            String imageFileName = servant.getPic();
            File file = new File(FOLDER_SERV_PICS + "/" + imageFileName);
            if (file.exists()) {
                Image image = new Image(file.toURI().toString());
                imageViewServant.setImage(image);
            } else {
                Alert alert = new Alert(AlertType.INFORMATION, "Picture not found !!!");
                alert.showAndWait();
            }
        }
    }
}
