package fatego;

import Tablas.Craftessence;
import Tablas.Servant;
import static fatego.CEEditController.FOLDER_CE_PICS;
import static fatego.ServantEditController.FOLDER_SERV_PICS;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * FXML Controller class
 */
public class ServantViewController implements Initializable {

    public static final String FOLDER_CLASS_PICS = "ClassPics";

    private Servant selectedServant;
    private Craftessence selectedCE;
    private EntityManager entityManager;
    private Servant servant;

    @FXML
    private AnchorPane rootServantView;
    @FXML
    private TableView<Servant> tableViewServants;
    @FXML
    private TableColumn<Servant, String> columnServant;
    @FXML
    private TableColumn<Servant, String> columnClass;
    @FXML
    private TableColumn<Servant, String> columnLevel;
    @FXML
    private TableColumn<Servant, String> columnCraft;
    @FXML
    private Label labelName;
    @FXML
    private Label labelClass;
    @FXML
    private Label labelAscesion;
    @FXML
    private Label labelLevel;
    @FXML
    private Label labelNP;
    @FXML
    private Label labelAquired;
    @FXML
    private Label labelCEName;
    @FXML
    private TextArea labelEffect;
    @FXML
    private ImageView imageViewServant;
    @FXML
    private ImageView imageViewCE;
    @FXML
    private ImageView imageViewClass;
    @FXML
    private Label labelNPLevel;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        columnServant.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnLevel.setCellValueFactory(new PropertyValueFactory<>("level"));
        columnClass.setCellValueFactory(
                cellData -> {
                    SimpleStringProperty property = new SimpleStringProperty();
                    if (cellData.getValue().getClass1() != null) {
                        property.setValue(cellData.getValue().getClass1().getNombre());
                    }
                    return property;
                });
        columnCraft.setCellValueFactory(
                cellData -> {
                    SimpleStringProperty property = new SimpleStringProperty();
                    if (cellData.getValue().getCraftessence() != null) {
                        property.setValue(cellData.getValue().getCraftessence().getName());
                    }
                    return property;
                });

        tableViewServants.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    selectedServant = newValue;
                    if (selectedServant != null) {
                        labelName.setText(selectedServant.getName());
                        labelClass.setText(selectedServant.getClass1().getNombre());
                        labelAscesion.setText(selectedServant.getAscension().toString());
                        labelLevel.setText(selectedServant.getLevel());
                        labelNP.setText(selectedServant.getNp());
                        labelNPLevel.setText(selectedServant.getNplevel().toString());
                        labelCEName.setText(selectedServant.getCraftessence().getName());
                        labelEffect.setText(selectedServant.getCraftessence().getEffect());
                            
                        
                        if (selectedServant.getPic() != null) {
                            String imageFileName = selectedServant.getPic();
                            File file = new File(FOLDER_SERV_PICS + "/" + imageFileName);
                            if (file.exists()) {
                                Image image = new Image(file.toURI().toString());
                                imageViewServant.setImage(image);
                            } else {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Picture not found !!!");
                                alert.showAndWait();
                            }
                        } else {
                            String imageFileName = selectedServant.getPic();
                            File file = new File(FOLDER_SERV_PICS + "/" + imageFileName);
                            Image image = new Image(file.toURI().toString());
                            imageViewServant.setImage(null);
                        }

                        if (selectedServant.getCraftessence().getPicCE() != null) {
                            String imageFileNameCE = selectedServant.getCraftessence().getPicCE();
                            File file = new File(FOLDER_CE_PICS + "/" + imageFileNameCE);
                            if (file.exists()) {
                                Image imageCE = new Image(file.toURI().toString());
                                imageViewCE.setImage(imageCE);
                            } else {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Picture not found !!!");
                                alert.showAndWait();
                            }
                        } else {
                            String imageFileNameCE = selectedServant.getPic();
                            File file = new File(FOLDER_SERV_PICS + "/" + imageFileNameCE);
                            Image imageCE = new Image(file.toURI().toString());
                            imageViewCE.setImage(null);
                        }

                        if (selectedServant.getClass1().getEmblem() != null) {
                            String imageFileNameClass = selectedServant.getClass1().getEmblem();
                            File fileClass = new File(FOLDER_CLASS_PICS + "/" + imageFileNameClass);
                            if (fileClass.exists()) {
                                Image imageClass = new Image(fileClass.toURI().toString());
                                imageViewClass.setImage(imageClass);
                            } else {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Picture not found !!!");
                                alert.showAndWait();
                            }
                        } else {
                            String imageFileNameClass = selectedServant.getCraftessence().getPicCE();
                            File fileClass = new File(FOLDER_CLASS_PICS + "/" + imageFileNameClass);
                            Image imageClass = new Image(fileClass.toURI().toString());
                            imageViewClass.setImage(null);
                        }
                        labelAquired.setText(selectedServant.getAquired().toString());
                        
                    } else {
                        labelName.setText("");
                        labelClass.setText("");
                        labelAscesion.setText("");
                        labelLevel.setText("");
                        labelNP.setText("");
                        labelNPLevel.setText("");
                        labelCEName.setText("");
                        labelEffect.setText("");
                        labelAquired.setText("");
                    }
                }
        );
    }

    public void loadAllServants() {
        Query queryServantFindAll = entityManager.createNamedQuery("Servant.findAll");
        List<Servant> listServant = queryServantFindAll.getResultList();
        tableViewServants.setItems(FXCollections.observableArrayList(listServant));
    }

    @FXML
    private void onActionButtonNew(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ServantEdit.fxml"));
            Parent rootServantEdit = fxmlLoader.load();

            ServantEditController servantEditController = (ServantEditController) fxmlLoader.getController();
            servantEditController.setTableViewPrevious(tableViewServants);
            servantEditController.setRootServantsView(rootServantView);
            selectedServant = new Servant();
            servantEditController.setServant(entityManager, selectedServant, true);
            servantEditController.showServantData();
            
            rootServantView.setVisible(false);

            StackPane rootMain = (StackPane) rootServantView.getScene().getRoot();
            rootMain.getChildren().add(rootServantEdit);

        } catch (IOException ex) {
            Logger.getLogger(ServantViewController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onActionButtonEdit(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ServantEdit.fxml"));
            Parent rootServantEdit = fxmlLoader.load();

            ServantEditController servantEditController = (ServantEditController) fxmlLoader.getController();
            servantEditController.setRootServantsView(rootServantView);
            servantEditController.setTableViewPrevious(tableViewServants);
            servantEditController.setServant(entityManager, selectedServant, false);
            servantEditController.showServantData();
            
            rootServantView.setVisible(false);

            StackPane rootMain = (StackPane) rootServantView.getScene().getRoot();
            rootMain.getChildren().add(rootServantEdit);

        } catch (IOException ex) {
            Logger.getLogger(ServantViewController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onActionButtonDelete(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm");
        alert.setHeaderText("Really want to scrap this servant ?");
        alert.setContentText(selectedServant.getName());
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            entityManager.getTransaction().begin();
            entityManager.merge(selectedServant);
            entityManager.remove(selectedServant);
            entityManager.getTransaction().commit();

            tableViewServants.getItems().remove(selectedServant);

            tableViewServants.getFocusModel().focus(null);
            tableViewServants.requestFocus();
        } else {
            int numFilaSeleccionada = tableViewServants.getSelectionModel().getSelectedIndex();
            tableViewServants.getItems().set(numFilaSeleccionada, selectedServant);
            TablePosition pos = new TablePosition(tableViewServants, numFilaSeleccionada, null);
            tableViewServants.getFocusModel().focus(pos);
            tableViewServants.requestFocus();
        }
    }

    @FXML
    private void onActionButtonCraft(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CEView.fxml"));
            Parent rootCEView = fxmlLoader.load();

            CEViewController CEView = (CEViewController) fxmlLoader.getController();
            CEView.setEntityManager(entityManager);
            CEView.loadAllCEs();

            rootServantView.setVisible(false);

            Pane rootMain = (Pane) rootServantView.getScene().getRoot();
            rootMain.getChildren().add(rootCEView);

        } catch (IOException ex) {
            Logger.getLogger(ServantViewController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

}
